# Prueba tecnica Front Ecomsur 2021

Esta prueba se uso redux con el ducks modular pattern para poder guardar los datos al momento de navegar, el carro de compras consta de un menu para poder llegar facilmente a las deferentes paginas dentro de la web, cuanta con Product List Page en el home, Product Display Page al momento de dar click al nombre de un producto, Cart Page donde mostramos los item agregados al carrito y Mini cart que muestra el numero de items dentro del carrito. 

## Instalar y Correr la aplicación

Instalar API (backend) y la aplicacion React (front):

1. En la carpeta `root` de la aplicacion correr:
   `npm install`
2. Navega al directorio `front` y vuelve a correr el comando:
   `npm install`
3. Regresa al directorio root `cd ..`.

La aplicación está compuesta de un servidor Express y una instalación básica de Create-React-App (CRA). Todo está configurado para correr con un solo comando

`npm run dev`

Esto correrá ambas aplicaciones (Express y CRA) al mismo tiempo.

- CRA se encuentra en:
  `http://localhost:3000/`
 y se ve de la siguiente forma:
 ![Running app](/example.png)


- El servidor se encuentra en:
  `http://localhost:5000/`

- La lista de productos se encuentra:
  `http://localhost:5000/api/products`

- Puedes encontrar cada producto por su ID:
  `http://localhost:5000/api/products/1`

- Las imágenes se encuentran en:|
  `http://localhost:5000/images/{{nombre-de-la-imagen}}`

