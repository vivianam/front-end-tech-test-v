import {createStore, combineReducers, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'

import { shopping_cart } from './shopping-ducks'

const storeReducer = combineReducers({
    productos: shopping_cart
})

export default function generateStore() {
    const store = createStore( storeReducer, composeWithDevTools( applyMiddleware(thunk) ) )
    return store
}