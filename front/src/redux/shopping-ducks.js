const shopping_cart_initial = {
    items: [],
    addItems: [],
    itempage: [],
    cant: 0
}

//types
export const BUY_ITEM = "BUY_ITEM";
export const REMOVE_ITEM = "REMOVE_ITEM";
export const PAGE_ITEM = "PAGE_ITEM";
export const ALL_ITEM = "ALL_ITEM";

function updateItem (add) {
    if(add.add !== undefined) {
         add.add = (add.add + 1)
         return itemSinStock(add)
    }else {
        return add.add = 1;
    }
}

const itemSinStock = (item) => {
    if(item.add > item.countInStock) {
        document.querySelector(`.card-items[id="${item['_id']}"] .card-add`).textContent = "Sin Stock"
        document.querySelector(`.card-items[id="${item['_id']}"] .card-add`).className = "card-add disable"
    }else {
        document.querySelector(`.card-items[id="${item['_id']}"] .card-add`).className = "card-add"
        document.querySelector(`.card-items[id="${item['_id']}"] .card-add`).textContent = "Agregar al carro"
    }
}

const itemRemove = (item) => {
    let cantnew = 0
    item.forEach(item => cantnew += item.add)
    return cantnew
}

// reducer
export const shopping_cart = (state = shopping_cart_initial, action) => {
    switch (action.type) {
        case ALL_ITEM : {
            return {
                ...state,
                items: action.payload,
            }
        }
        case BUY_ITEM : {
            let index = state.addItems.findIndex(el => el["_id"] === action.payload["_id"]);

            if(index === -1) {
                updateItem(action.payload)
                return {
                    ...state,
                   
                    addItems: [...state.addItems,action.payload],
                    cant: (state.cant + 1)
                }
            }else {
                updateItem(action.payload)
                return {...state,
                    cant: (state.cant + 1)
                };
            }
        }
        case REMOVE_ITEM : {
            let newarr = state.addItems.filter(e => e["_id"] !== action.payload["_id"]);
            return {
                ...state,
                addItems: newarr,
                cant: itemRemove(newarr)
            }
        }
        case PAGE_ITEM : {
            return {
                ...state,
                itempage: action.payload,
            }
        }
        default : 
            return state;
    }
}

// actions the
export const allapi = () => async (dispatch, getState) => {
    try {
        fetch('http://localhost:5000/api/products')
        .then((res) => res.json())
        .then((res) => {
            dispatch({
                type: ALL_ITEM,
                payload: res
            })
        })
    } catch (error) {
        
    }
}

export const buy_item_function = (item) => {
    return {
        type: BUY_ITEM,
        payload: item
    }
}

export const remove_item_function = (item) => {
    return {
        type: REMOVE_ITEM,
        payload: item
    }
}

export const page_item_function = (item) => {
    return {
        type: PAGE_ITEM,
        payload: item
    }
}
