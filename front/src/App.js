import React, { useEffect, useState } from 'react'
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Navbar from './components/Navbar/Navbar'
import Home from './components/Home'
import Cart from './components/Cart'
import Producto from './components/Producto'

const App = () => {
  // -------------------------------------------------
  // DO NOT USE THE CODE BELOW FROM LINES 8 TO 18. THIS IS
  // HERE TO MAKE SURE THAT THE EXPRESS SERVER IS RUNNING
  // CORRECTLY. DELETE CODE WHEN COMPLETING YOUR TEST.

  // -------------------------------------------------

  return (
    <div style={{ textAlign: 'center' }}>
      <BrowserRouter>
            <div className="App">
            
              <Navbar/>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/cart" component={Cart}/>
                    <Route exact path="/product/:product_id" component={Producto} />
                  </Switch>
             </div>
       </BrowserRouter>
    </div>
  )
}

export default App
