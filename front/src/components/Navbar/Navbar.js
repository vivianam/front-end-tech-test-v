import React from 'react';
import { Link } from 'react-router-dom'
import {useSelector} from 'react-redux'
 const Navbar = () =>{
    const cant = useSelector(store => store.productos.cant)
    return(
            <nav className="nav">
                <div className="container">
                    <Link to="/" className="brand-logo">Shopping</Link>
                    
                    <ul className="right">
                        <li><Link to="/cart">carrito ({cant})</Link></li>
                    </ul>
                </div>
            </nav>  
    )
}

export default Navbar;