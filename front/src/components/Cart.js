import React from 'react';
import {useDispatch, useSelector} from 'react-redux'
import { remove_item_function } from '../redux/shopping-ducks'

const Cart = () => {
    // -------------------------------------------------
    // DO NOT USE THE CODE BELOW FROM LINES 8 TO 18. THIS IS
    // HERE TO MAKE SURE THAT THE EXPRESS SERVER IS RUNNING
    // CORRECTLY. DELETE CODE WHEN COMPLETING YOUR TEST.
    // -------------------------------------------------
    const dispatch = useDispatch()
    const productos = useSelector(store => store.productos.addItems)

    const removeItem = (item) => {
        console.log('dasd', item)
        dispatch(remove_item_function(item))
    }

    return (
        <div className="container-items cart" style={{ textAlign: 'center' }}>
        {productos && productos.map((producto) => (
            <div className="card-items" id={producto["_id"]}>
                <div className="card-name">{producto.name}</div>
                <div className="card-image">
                    <img src={'http://localhost:5000'+producto.image} alt="alt"/>
                </div>
                <p>Items Añadidos: {producto.add}</p>
                <div className="card-add" onClick={() => removeItem(producto)}>Remove item</div>
                <div className="card-price">$ {producto.price}</div>
            </div>
        ))}
      </div>
    )
  }
  
  export default Cart