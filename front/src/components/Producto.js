import React from 'react';
import {useDispatch, useSelector} from 'react-redux'
import { buy_item_function } from '../redux/shopping-ducks'

const Producto = (item) => {
    // -------------------------------------------------
    // DO NOT USE THE CODE BELOW FROM LINES 8 TO 18. THIS IS
    // HERE TO MAKE SURE THAT THE EXPRESS SERVER IS RUNNING
    // CORRECTLY. DELETE CODE WHEN COMPLETING YOUR TEST.
    // -------------------------------------------------
    const dispatch = useDispatch()
    const producto = useSelector(store => store.productos.itempage)
    const addtocart = (item) => {
        dispatch(buy_item_function(item))
    }

    return (
        <div className="container-items producto" style={{ textAlign: 'center' }}>
            <div className="card-image">
                    <img src={'http://localhost:5000'+producto.image} alt="alt"/>
            </div>
            <div className="card-items" id={producto["_id"]}>
                
                <div className="card-name" >{producto.name}</div>
                <div className="card-add" onClick={() => addtocart(producto)}>Agregar al carro</div>
                <div className="card-price">$ {producto.price}</div>
            </div>
        {console.log(producto)}
      </div>
    )
  }
  
  export default Producto