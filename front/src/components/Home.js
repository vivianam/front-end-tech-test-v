import React, { useEffect } from 'react'
import {useDispatch, useSelector} from 'react-redux'
import { allapi, buy_item_function, page_item_function } from '../redux/shopping-ducks'

const Home = ({history}) => {
    
    const dispatch = useDispatch()
    const productos = useSelector(store => store.productos.items)


    useEffect(() => {
        dispatch(allapi())
    }, [])

    const addtocart = (item) => {
       /*  if(item.add) {
           
        }else {
            console.log("elsee")
             document.querySelector(`.card-items[id="${item['_id']}"] .card-add`).textContent = "Agregar al carro"
        } */
        dispatch(buy_item_function(item))
    }
    const gopage = (item) => {
        dispatch(page_item_function(item))
        history.push(`/product/${item["_id"]}`)
    }

    return (
      <div className="container-items home" style={{ textAlign: 'center' }}>
        {productos && productos.map((producto) => (
            <div className="card-items " id={producto["_id"]}>
                <div className="card-name" onClick={() => gopage(producto)}>{producto.name}</div>
                <div className="card-image">
                    <img src={'http://localhost:5000'+producto.image} alt="alt"/>
                </div>
                <div className="card-add" onClick={() => addtocart(producto)}>Agregar al carro</div>
                <div className="card-price">$ {producto.price}</div>
            </div>
            
        ))}
    
      </div>
    )
  }
  
  export default Home